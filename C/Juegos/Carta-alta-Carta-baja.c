#include <stdio.h>
#include <stdlib.h>

int main()
{
    signed int carta1, carta2;
    char option;
//Esta línea es incompatible con la versión C99 del compilador de C...
    srand(time(NULL));
    carta1 = 1 + (rand() % 12);
    carta2 = 1 + (rand() % 12);
    
    printf("\n%d\n",carta1);
    printf("¿Alta (A), o Baja (B)?\n");
    scanf("%c",&option);
    
    switch(option){
        case 'A':
            if(carta1 > carta2){
                printf("%u  %u\n",carta1, carta2);
                printf("\nHas ganado!");
            }
            else if(carta1 == carta2)
                printf("\nEMPATE");
            else
                printf("Prueba de nuevo...");
            break;
        case 'B':
            if(carta1 < carta2){
                printf("%d  %d\n",&carta1, &carta2);
                printf("\nHas ganado!");
            }
            else if(carta1 == carta2)
                printf("\nEMPATE");
            else
                printf("Prueba de nuevo...");
            break;
        default:
            do{
                printf("GAMEOVER");
            }while(1<0);
            break;
    }
    return 0;
}
