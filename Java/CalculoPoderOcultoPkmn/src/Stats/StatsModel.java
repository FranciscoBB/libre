package Stats;
/**
 *
 * @author Francisco Barraza
 */

public class StatsModel {

private int puntosSalud;
private int ataque;
private int defensa;
private int ataqEsp;
private int defEsp;
private int velocidad;

public StatsModel(){
    
}

public StatsModel(int puntosSalud, int ataque, int defensa, int ataqEsp, int defEsp, int velocidad) {
    this.puntosSalud = puntosSalud;
    this.ataque = ataque;
    this.defensa = defensa;
    this.ataqEsp = ataqEsp;
    this.defEsp = defEsp;
    this.velocidad = velocidad;
}

    public int getPuntosSalud() {
        return puntosSalud;
    }

    public void setPuntosSalud(int puntosSalud) {
        this.puntosSalud = puntosSalud;
    }

    public int getAtaque() {
        return ataque;
    }

    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }

    public int getDefensa() {
        return defensa;
    }

    public void setDefensa(int defensa) {
        this.defensa = defensa;
    }

    public int getAtaqEsp() {
        return ataqEsp;
    }

    public void setAtaqEsp(int ataqEsp) {
        this.ataqEsp = ataqEsp;
    }

    public int getDefEsp() {
        return defEsp;
    }

    public void setDefEsp(int defEsp) {
        this.defEsp = defEsp;
    }

    public int getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
    }
}
