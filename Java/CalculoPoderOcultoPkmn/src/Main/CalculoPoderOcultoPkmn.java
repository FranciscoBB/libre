package Main;

import java.util.Scanner;

/**
 * Cálculo de Tipo de Poder Oculto
 * @author Francisco Barraza
 * 
 * Este algoritmo se ha sacado de https://pokemon.fandom.com
 */

import Stats.StatsModel;
import java.util.ArrayList;

public class CalculoPoderOcultoPkmn {

    public static void main(String[] args) {
        
        ArrayList<String> tipos = new ArrayList<>();
        tipos.add("LUCHA");
        tipos.add("VOLADOR");
        tipos.add("VENENO");
        tipos.add("TIERRA");
        tipos.add("ROCA");
        tipos.add("BICHO");
        tipos.add("FANTASMA");
        tipos.add("ACERO");
        tipos.add("FUEGO");
        tipos.add("AGUA");
        tipos.add("PLANTA");
        tipos.add("ELECTRICO");
        tipos.add("PSIQUICO");
        tipos.add("HIELO");
        tipos.add("DRAGON");
        tipos.add("SINIESTRO");
        
        int sumatorio = 0;
        int resultado = 0;        
        
        System.out.println("Bienvenido al programa de cálculo de tipo de Poder Oculto de Pokémon.\n----------------------------------------------------------------------------------\n\n\n");
        System.out.println("Indica los stats que se solicitarán a continuación:");
        
        Scanner sc = new Scanner(System.in);
        StatsModel puntosPkmn = new StatsModel();
      
        System.out.println("Indica cuantos PS tiene tu PKMN: ");
        puntosPkmn.setPuntosSalud(sc.nextInt());
        System.out.println("");
        
        System.out.println("Indica cuantos puntos de ATAQUE tiene tu PKMN: ");
        puntosPkmn.setAtaque(sc.nextInt());
        System.out.println("");
        
        System.out.println("Indica cuantos puntos de DEFENSA tiene tu PKMN: ");
        puntosPkmn.setDefensa(sc.nextInt());
        System.out.println("");
        
        System.out.println("Indica cuantos puntos de ATQ.ESPEC tu PKMN: ");
        puntosPkmn.setAtaqEsp(sc.nextInt());
        System.out.println("");
        
        System.out.println("Indica cuantos puntos de DEF.ESPEC tu PKMN: ");
        puntosPkmn.setDefEsp(sc.nextInt());
        System.out.println("");
        
        System.out.println("Indica cuantos puntos de VELOCIDAD tiene tu PKMN: ");
        puntosPkmn.setVelocidad(sc.nextInt());
        System.out.println("");
        
        if(puntosPkmn.getPuntosSalud() %2 == 0){
            sumatorio+=0;
        } else {
            sumatorio+=1;
        }
        
        if(puntosPkmn.getAtaque() %2 == 0){
            sumatorio+=0;
        } else {
            sumatorio+=2;
        }
        
        if(puntosPkmn.getDefensa() %2 == 0){
            sumatorio+=0;
        } else {
            sumatorio+=4;
        }
        
        if(puntosPkmn.getAtaqEsp()%2 == 0){
            sumatorio+=0;
        } else {
            sumatorio+=8;
        }
        
        if(puntosPkmn.getDefEsp()%2 == 0){
            sumatorio+=0;
        } else {
            sumatorio+=16;
        }
        
        if(puntosPkmn.getVelocidad() %2 == 0){
            sumatorio+=0;
        } else {
            sumatorio+=32;
        }
        
        resultado = (sumatorio * 15) / 63;
        
        System.out.println("El tipo del movimiento PODER OCULTO ES: "+tipos.get(resultado)+"");
        System.out.println("");
        puntosPkmn = null;
        tipos.clear();
        sumatorio = 0;
        
        System.exit(0);
    }
}
